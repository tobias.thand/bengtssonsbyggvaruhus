package verktyg;

import java.util.Objects;

/**
 * Klass som representerar kvantiteter av olika slag.
 */
public class Kvantitet {
    private final int kvantitet;

    /**
     * Konstruktion av kvantitet i form av heltal.
     * @param kvantitet En kvantitet.
     */
    public Kvantitet(int kvantitet){
        this.kvantitet = kvantitet;
    }

    /**
     * Skaffar fram och returnerar kvantitet.
     * @return En kvantitets värde.
     */
    public int getKvantitet(){
        return kvantitet;
    }

    /**
     * Omvandlar en kvantitet till text (String)
     * @return <code>Kvantitet</code> som <code>String</code>
     */
    @Override
    public String toString(){
        return Integer.toString(kvantitet);
    }

    /**
     * Jämför två instanser av klassen Kvantitet.
     * @param objekt En kvantitet.
     * @return <code>false</code> om objektet är intigt,
     *       om de två intanserna är identiska reurneras <code>true</code>
     *       annars returneras <code>false</code>
     */
    @Override
    public boolean equals(Object objekt){
        if(objekt == null){
            return false;
        }
        if(getClass() != objekt.getClass()){
            return false;
        }
        final Kvantitet ovrig = (Kvantitet) objekt;
        if(!Objects.equals(this.kvantitet, ovrig.kvantitet)){
            return false;
        }
        return true;
    }

    /**
     * Adderar en {@link Kvantitet} med en annan {@link Kvantitet} genom <code>Kvantitet</code>
     * @param ovrig En övrig <code>Kvantitet</code>
     * @return Summan av <code>Kvantitet</code> adderat med en övrig <code>Kvantitet</code>
     */
    public Kvantitet addition (Kvantitet ovrig){
        return new Kvantitet(this.kvantitet + ovrig.kvantitet);
    }

    /**
     * Subtraherar en {@link Kvantitet} med en annan {@link Kvantitet} genom <code>Kvantitet</code>
     * @param ovrig En övrig <code>Kvantitet</code>
     * @return Differens av <code>Kvantitet</code> subtraherat med en övrig <code>Kvantitet</code>
     */
    public Kvantitet subtraktion (Kvantitet ovrig){
        return new Kvantitet(this.kvantitet - ovrig.kvantitet);
    }

    /**
     * Multiplicerar en {@link Kvantitet} med en annan {@link Kvantitet} genom <code>Kvantitet</code>
     * @param ovrig En övrig <code>Kvantitet</code>
     * @return Produkten av <code>Kvantitet</code> subtraherat med en övrig <code>Kvantitet</code>
     */
    public Kvantitet multiplikation(Kvantitet ovrig){
        return new Kvantitet(this.kvantitet * ovrig.kvantitet);
    }
}

package verktyg;

import java.util.Objects;

/**
 * Klass för identifikation av kund.
 */
public class KundID {
    private String kundensIDNummer;

    /**
     * Konstruktion av KundID.
     * @param kundensIDNummer Ett ID-nummer.
     */
    public KundID(String kundensIDNummer){
        this.kundensIDNummer = kundensIDNummer;
    }

    /**
     * Skaffar fram kundens ID-nummer i form av text (String).
     * @return ID-mummret i form av text (String).
     */
    public String getKundensIDNummer(){ return kundensIDNummer; }

    /**
     * Jämför två instanser av klassen KundID.
     * @param objekt Den övriga instansen.
     * @return <code>false</code> om objektet är intigt,
     *             om de två intanserna är identiska reurneras <code>true</code>
     *             annars returneras <code>false</code>
     */
    @Override
    public boolean equals(Object objekt){
        if (objekt == null){
            return false;
        }
        if (getClass() != objekt.getClass()){
            return false;
        }
        final KundID ovrig = (KundID) objekt;
        if(!Objects.equals(this.kundensIDNummer, ovrig.kundensIDNummer)){
            return false;
        }
        return true;
    }
}

package verktyg;

/**
 * Motsvarar system för rabatter
 */
public class Rabatt {
    private double procentRabatt;
    private final double UTAN_RABATT = 1.0;

    /**
     * Konstruerar en instans då ingen rabatt appliceras.
     */
    public Rabatt(){
        this.procentRabatt = UTAN_RABATT;
    }

    /**
     * Konstruerar rabatt med en given procent.
     * @param procentRabatt En rabatt i procent på ett köp.
     */
    public Rabatt(double procentRabatt){
        this.procentRabatt = procentRabatt;
    }

    /**
     * Skaffar fram rabatt-procent.
     * @return rabatt-procent.
     */
    public double getProcentRabatt(){
        return procentRabatt;
    }
}


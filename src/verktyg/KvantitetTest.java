package verktyg;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;



class KvantitetTest {
    private Kvantitet likaMedSju;

    @BeforeEach
    void setUp() {
        likaMedSju = new Kvantitet(7);
    }

    @AfterEach
    void tearDown() {
        likaMedSju = null;
    }

    @Test
    void testToString() {
        int siffra = 233;
        Kvantitet tal = new Kvantitet(siffra);
        String presumeratResultat = Integer.toString(siffra);
        String faktisktResultat = tal.toString();
        assertEquals(presumeratResultat, faktisktResultat, "Texten stämmer ej överns med siffran");
    }

    @Test
    void testToStringNoll() {
        int siffra = 0;
        Kvantitet tal = new Kvantitet(siffra);
        String presumeratResultat = Integer.toString(siffra);
        String faktisktResultat = tal.toString();
        assertEquals(presumeratResultat, faktisktResultat, "Texten stämmer ej överns med siffran");
    }

    @Test
    void testToStringNegativtTal() {
        int siffra = -233;
        Kvantitet tal = new Kvantitet(siffra);
        String presumeratResultat = Integer.toString(siffra);
        String faktisktResultat = tal.toString();
        assertEquals(presumeratResultat, faktisktResultat, "Texten stämmer ej överns med siffran");
    }

    @Test
    void testToStringMAXVALUE() {
        int siffra = Integer.MAX_VALUE;
        Kvantitet tal = new Kvantitet(siffra);
        String presumeratResultat = Integer.toString(siffra);
        String faktisktResultat = tal.toString();
        assertEquals(presumeratResultat, faktisktResultat, "Texten stämmer ej överns med siffran");
    }

    @Test
    void testToStringMINVALUE() {
        int siffra = Integer.MIN_VALUE;
        Kvantitet tal = new Kvantitet(siffra);
        String presumeratResultat = Integer.toString(siffra);
        String faktisktResultat = tal.toString();
        assertEquals(presumeratResultat, faktisktResultat, "Texten stämmer ej överns med siffran");
    }

    @Test
    void testEquals() {
        int sju = 7;
        Kvantitet kvantitetSju = new Kvantitet(sju);
        boolean presumeratResultat = true;
        boolean faktisktResultat = kvantitetSju.equals(kvantitetSju);
        assertEquals(presumeratResultat, faktisktResultat, "Siffran är ej lika med sju");
    }

    @Test
    void testEqualsEjLika() {
        int nio = 9;
        Kvantitet kvantitetNio = new Kvantitet(nio);
        boolean presumeratResultat = false;
        boolean faktisktResultat = likaMedSju.equals(kvantitetNio);
        assertEquals(presumeratResultat, faktisktResultat, "Siffran är lika med sju");
    }

    @Test
    void testEqualsEjLikaMedObjekt() {
        Object objekt = new Object();
        boolean presumeratResultat = false;
        boolean faktisktResultat = likaMedSju.equals(objekt);
        assertEquals(presumeratResultat, faktisktResultat, "Siffran är lika med java-objektet");
    }

    @Test
    void testEqualsEjLikaMedNull() {
        Object objekt = null;
        boolean presumeratResultat = false;
        boolean faktisktResultat = likaMedSju.equals(objekt);
        assertEquals(presumeratResultat, faktisktResultat, "Siffran är lika med null");
    }

    @Test
    void addition() {
        int sju = 7;
        int nio = 9;
        int sjuPlusnio = 16;
        Kvantitet kvantitetSju = new Kvantitet(sju);
        Kvantitet kvantitetNio = new Kvantitet(nio);
        Kvantitet presumeratResultat = new Kvantitet(sjuPlusnio);
        Kvantitet faktisktResultat = kvantitetSju.addition(kvantitetNio);
        assertEquals(presumeratResultat, faktisktResultat, "Summan stämmer ej");
    }

    @Test
    void additionNegativtTal() {
        int sju = 7;
        int minusNio = -9;
        int sjuMinusNio = -2;
        Kvantitet kvantitetSju = new Kvantitet(sju);
        Kvantitet kvantitetMinusNio = new Kvantitet(minusNio);
        Kvantitet presumeratResultat = new Kvantitet(sjuMinusNio);
        Kvantitet faktisktResultat = kvantitetSju.addition(kvantitetMinusNio);
        assertEquals(presumeratResultat, faktisktResultat, "Summan stämmer ej");
    }

    @Test
    void additionLikaMedNoll() {
        int sju = 7;
        int minusSju = -7;
        int sjuMinusSju = 0;
        Kvantitet kvantitetSju = new Kvantitet(sju);
        Kvantitet kvantitetMinusSju = new Kvantitet(minusSju);
        Kvantitet presumeratResultat = new Kvantitet(sjuMinusSju);
        Kvantitet faktisktResultat = kvantitetSju.addition(kvantitetMinusSju);
        assertEquals(presumeratResultat, faktisktResultat, "Summan är ej lika med noll");
    }

    @Test
    void additionMaxMinusMin() {
        int max = Integer.MAX_VALUE;
        int min = Integer.MIN_VALUE;
        int maxMinusMin = -1;
        Kvantitet kvantitetMax = new Kvantitet(max);
        Kvantitet kvantitetMin = new Kvantitet(min);
        Kvantitet presumeratResultat = new Kvantitet(maxMinusMin);
        Kvantitet faktisktResultat = kvantitetMax.addition(kvantitetMin);
        assertEquals(presumeratResultat, faktisktResultat, "Summan är ej lika med -1");
    }

    @Test
    void subtraktion() {
        int sju = 7;
        int nio = 9;
        int sjuMinusNio = -2;
        Kvantitet kvantitetSju = new Kvantitet(sju);
        Kvantitet kvantitetNio = new Kvantitet(nio);
        Kvantitet presumeratResultat = new Kvantitet(sjuMinusNio);
        Kvantitet faktisktResultat = kvantitetSju.subtraktion(kvantitetNio);
        assertEquals(presumeratResultat, faktisktResultat, "Resultatet är ej lika med -2");
    }

    @Test
    void subtraktionMedNegativtTalPositivtResultat() {
        int sju = 7;
        int minusNio = -9;
        int sjuMinusMinusNio = 16;
        Kvantitet kvantitetSju = new Kvantitet(sju);
        Kvantitet kvantitetMinusNio = new Kvantitet(minusNio);
        Kvantitet presumeratResultat = new Kvantitet(sjuMinusMinusNio);
        Kvantitet faktisktResultat = kvantitetSju.subtraktion(kvantitetMinusNio);
        assertEquals(presumeratResultat, faktisktResultat, "Resultatet är ej lika med 16");
    }

    @Test
    void subtraktionPositivtResultat() {
        int sju = 7;
        int tre = 3;
        int sjuMinusTre = 4;
        Kvantitet kvantitetSju = new Kvantitet(sju);
        Kvantitet kvantitetTre = new Kvantitet(tre);
        Kvantitet presumeratResultat = new Kvantitet(sjuMinusTre);
        Kvantitet faktisktResultat = kvantitetSju.subtraktion(kvantitetTre);
        assertEquals(presumeratResultat, faktisktResultat, "Resultatet är ej lika med 4");
    }

    @Test
    void subtraktionMedNollResultat() {
        int sju = 7;
        int minusSju = 7;
        int sjuMinusSju = 0;
        Kvantitet kvantitetSju = new Kvantitet(sju);
        Kvantitet kvantitetMinusSju = new Kvantitet(minusSju);
        Kvantitet presumeratResultat = new Kvantitet(sjuMinusSju);
        Kvantitet faktisktResultat = kvantitetSju.subtraktion(kvantitetMinusSju);
        assertEquals(presumeratResultat, faktisktResultat, "Resultatet är ej lika med 0");
    }

    @Test
    void multiplikation() {
        int sju = 7;
        int nio = 9;
        int sextiotre = 63;
        Kvantitet kvantitetSju = new Kvantitet(sju);
        Kvantitet kvantitetNio = new Kvantitet(nio);
        Kvantitet presumeratResultat = new Kvantitet(sextiotre);
        Kvantitet faktisktResultat = kvantitetSju.multiplikation(kvantitetNio);
        assertEquals(presumeratResultat, faktisktResultat, "Produkten är ej lika med 63");
    }

    @Test
    void multiplikationNegativtTal() {
        int sju = 7;
        int minusNio = -9;
        int minusSextiotre = -63;
        Kvantitet kvantitetSju = new Kvantitet(sju);
        Kvantitet kvantitetMinusNio = new Kvantitet(minusNio);
        Kvantitet presumeratResultat = new Kvantitet(minusSextiotre);
        Kvantitet faktisktResultat = kvantitetSju.multiplikation(kvantitetMinusNio);
        assertEquals(presumeratResultat, faktisktResultat, "Produkten är ej lika med -63");
    }

    @Test
    void multiplikationNegativaTal() {
        int minusSju = -7;
        int minusNio = -9;
        int sextiotre = 63;
        Kvantitet kvantitetMinusSju = new Kvantitet(minusSju);
        Kvantitet kvantitetMinusNio = new Kvantitet(minusNio);
        Kvantitet presumeratResultat = new Kvantitet(sextiotre);
        Kvantitet faktisktResultat = kvantitetMinusSju.multiplikation(kvantitetMinusNio);
        assertEquals(presumeratResultat, faktisktResultat, "Produkten är ej lika med 63");
    }

    @Test
    void multiplikationMedNoll() {
        int sju = 7;
        int noll = 0;
        int sjuXNoll = 0;
        Kvantitet kvantitetSju = new Kvantitet(sju);
        Kvantitet kvantitetNoll = new Kvantitet(noll);
        Kvantitet presumeratResultat = new Kvantitet(sjuXNoll);
        Kvantitet faktisktResultat = kvantitetSju.multiplikation(kvantitetNoll);
        assertEquals(presumeratResultat, faktisktResultat, "Produkten är ej lika med 0");
    }
}

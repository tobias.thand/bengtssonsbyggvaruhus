package kontroll;

import modell.Kassa;
import modell.Betalning;
import modell.Kvitto;
import modell.Kop;
import verktyg.Kvantitet;
import integrering.*;

/**
 *  Samtliga operationer sker genom denna controller.
 */
public class Kontroll {
    private Bokforing bokforing;
    private Kassa kassa;
    private Kop kop;
    private Lager lager;
    private RabattRegister rabattRegister;
    private Utskrift utskrift;
    private VaruRegister varuRegister;

    /**
     * @param bildaRegister Används för att nå de klasser som hanterar anrop från en databas.
     * @param bildaSystem Används för att nå klassernas externa system-anrop.
     * @param utskrift Används som gränssnitt för en skrivare.
     */
    public Kontroll(BildaSystem bildaSystem, Utskrift utskrift, BildaRegister bildaRegister){
        this.bokforing = bildaSystem.getBokforing();
        this.lager = bildaSystem.getLager();
        this.rabattRegister = bildaRegister.getRabattRegister();
        this.varuRegister = bildaRegister.getVaruRegister();
        this.utskrift = utskrift;
        this.kassa = new Kassa();
    }

    /**
     * Påbörjar ett nytt köp.
     */
    public void nyttKop(){
        this.kop = new Kop();
    }

    /**
     * Varans uppgifter och antal registreras.
     * @param antal En given varas antal.
     * @param varuID Vara som ska registreras vid ett köp.
     * @return Om {@link Vara} är disponibel returneras uppgifterna i form av text (String),
     * annars returneras köpets summerade belopp i form av text (String).
     */
    public String registreraVara (String varuID, Kvantitet antal){
        if (varuRegister.disponibelVara(varuID)){
            Vara vara = varuRegister.getVara(varuID, antal);
            return kop.uppdateraKop(vara) + ", antalet av varan: "
                    + antal.toString() + "\n" +
                    ", totalt: " + visaTotaltBelopp();
        }
        return ", totalt: " + visaTotaltBelopp();
    }

    /**
     * Visar det totala beloppet som text (String).
     * @return det totala beloppet som text (String).
     */
    public String visaTotaltBelopp(){
        return kop.getSumma().getSumma().toString();
    }

    /**
     * Visar den totala summan inklusive moms i form av text (String).
     * @return den totala summan inklusive moms i form av text (String).
     */
    public String visaTotaltInklMoms(){
        return "det totala belopppet inklusive moms: " + kop.getSumma().getSummaInklMoms().toString();
    }

    /**
     * Kundens inbetalade belopp registreras av "kassa" och bokas. Lagret status uppdateras.
     * Köpets information skapar ett nytt kvitto som skrivs ut.
     * "kop" töms.
     * @param betalatBelopp Det belopp som kunden ger till kassören som {@link Kvantitet}
     * @return Växeln - som betalas tillbaka till kunden - i from av text (String).
     */
    public String betalning(Kvantitet betalatBelopp) {
        Betalning betalning = new Betalning(betalatBelopp, kop.getSumma());
        kassa.bifogaBetalning(betalning);
        bokforing.bokning(kop);
        lager.uppdateraLager(kop);
        Kvitto kvitto = new Kvitto(kop);
        utskrift.skrivUtKvitto(kvitto);
        kop = null;
        return "Växel: " + betalning.getVaxel().toString();
    }
}


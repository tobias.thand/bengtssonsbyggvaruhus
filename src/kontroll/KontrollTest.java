package kontroll;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import integrering.Utskrift;
import integrering.BildaSystem;
import integrering.BildaRegister;
import verktyg.Kvantitet;

class KontrollTest {
    private Kontroll kontroll;

    @BeforeEach
    void setUp() {
        kontroll = new Kontroll(new BildaSystem(), new Utskrift(), new BildaRegister());
    }

    @AfterEach
    void tearDown() {
        kontroll = null;
    }

    @Test
    void registreraVara() {
        kontroll.nyttKop();
        String namnVara = "Yxa";
        Kvantitet pris = new Kvantitet(100);
        Kvantitet moms = new Kvantitet(25);
        Kvantitet antalet = new Kvantitet(3);
        String presumeratResultat = "varans namn: " + namnVara + "\t" +
                "varans pris: " + pris + "\n" +
                "varans moms: " + moms + "\t" + ", antalet av varan: " + antalet +
                "\n" + ", totalt: " + pris.multiplikation(antalet);
        String faktisktResultat = kontroll.registreraVara(namnVara, new Kvantitet(3));
        assertEquals(presumeratResultat, faktisktResultat,
                "Texten stämmer ej överns med texten ifrån VaruRegister");
    }

    @Test
    void visaTotaltInklMoms() {
        kontroll.nyttKop();
        String namnVara = "Yxa";
        Kvantitet pris = new Kvantitet(100);
        Kvantitet moms = new Kvantitet(25);
        Kvantitet antalet = new Kvantitet(3);
        kontroll.registreraVara(namnVara, antalet);
        Kvantitet betalatBelopp = new Kvantitet(500);
        String presumeratResultat = "Växel: " +
                betalatBelopp.subtraktion(pris.addition(moms).multiplikation(antalet));
        String faktisktResultat = kontroll.betalning(betalatBelopp);
        assertEquals(presumeratResultat, faktisktResultat,
                "Växelbeloppet stämmer ej");
    }

    @Test
    void betalning() {
        kontroll.nyttKop();
        String namnVara = "Yxa";
        Kvantitet pris = new Kvantitet(100);
        Kvantitet moms = new Kvantitet(25);
        Kvantitet antalet = new Kvantitet(3);
        kontroll.registreraVara(namnVara, antalet);
        Kvantitet betalatBelopp = new Kvantitet(500);
        String presumeratResultat = "Växel: "
                + betalatBelopp.subtraktion(pris.addition(moms).multiplikation(antalet));
        String faktisktResultat = kontroll.betalning(betalatBelopp);
        assertEquals(presumeratResultat, faktisktResultat,
                "Växelbeloppet stämmer ej");
    }
}
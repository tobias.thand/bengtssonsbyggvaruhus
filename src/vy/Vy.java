package vy;

import kontroll.Kontroll;
import verktyg.Kvantitet;

/**
 * Motsvarar en view-klass
 */
public class Vy {
    private Kontroll kontroll;

    /**
     * Konstruerar en instans av motsvarigheten till view.
     * @param kontroll Ifrån vilken anropen sker.
     */
    public Vy (Kontroll kontroll){
        this.kontroll = kontroll;
    }

    /**
     * Substitut för vad som sker när en användare (kassör) matar in uppgifter.
     */
    public void exempel(){
        System.out.println("Nytt köp påbörjas. \n");
        kontroll.nyttKop();
        System.out.println("Mata in varornas sträck-kod. \n");
        String out = kontroll.registreraVara("Yxa", new Kvantitet(199));
        System.out.println(out);
        out = kontroll.registreraVara("Morakniv", new Kvantitet(50));
        System.out.println(out);
        out = kontroll.registreraVara("Såg", new Kvantitet(299));
        System.out.println(out);
        out = kontroll.registreraVara("Spade", new Kvantitet(99));
        System.out.println(out);
        out = kontroll.registreraVara("Hammare", new Kvantitet(89));
        System.out.println(out);
        out = kontroll.betalning(new Kvantitet(1000));
        System.out.println(out);
    }
}


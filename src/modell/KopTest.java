package modell;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import integrering.Vara;
import integrering.DTO;
import verktyg.Kvantitet;

class KopTest {

    @Test
    void uppdateraKop() {
        Kvantitet pris = new Kvantitet(100);
        Kvantitet moms = new Kvantitet(25);
        String varansNamnID = "Yxa";
        Kvantitet antalet = new Kvantitet(3);
        DTO attributVara = new DTO(pris, varansNamnID, moms);
        Vara vara = new Vara(attributVara, varansNamnID, antalet);
        Kop kop = new Kop();
        String presumeratResultat = vara.getVaransAttribut().toString();
        String faktisktResultat = kop.uppdateraKop(vara);
        assertEquals(presumeratResultat, faktisktResultat, "Texten stämmer ej");
    }

    @Test
    void testToString() {
        Kvantitet pris = new Kvantitet(100);
        Kvantitet moms = new Kvantitet(25);
        String varansNamnID = "Yxa";
        Kvantitet antalet = new Kvantitet(3);
        DTO attributVara = new DTO(pris, varansNamnID, moms);
        Vara vara = new Vara(attributVara, varansNamnID, antalet);
        Kop kop = new Kop();
        kop.uppdateraKop(vara);
        String presumeratResultat = "varans namn: " + varansNamnID + "\t" +
                "varans pris: " + pris + "\n" +
                "varans moms: " + moms + "\t" + ", antalet av varan: " + antalet +
                "\n" + "Summa: " + pris.multiplikation(antalet) + "\n" + "Moms: " +
                moms.multiplikation(antalet) + "\n";
    }
}
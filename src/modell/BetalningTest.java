package modell;

import static org.junit.jupiter.api.Assertions.*;

import integrering.Vara;
import org.junit.jupiter.api.Test;
import integrering.DTO;
import verktyg.Kvantitet;

class BetalningTest {

    @Test
    void getVaxel() {
        Kvantitet pris = new Kvantitet(100);
        Kvantitet moms = new Kvantitet(25);
        String varansNamnID = "Yxa";
        Kvantitet antalet = new Kvantitet(3);
        DTO attributVara = new DTO(pris, varansNamnID, moms);
        Vara vara = new Vara(attributVara, varansNamnID, antalet);
        Resultat resultat = new Resultat();
        resultat.uppdateraSumma(vara);
        Kvantitet betalatBelopp = new Kvantitet(500);
        Betalning betalning = new Betalning(betalatBelopp, resultat);
        Kvantitet presumeratReslutat =
                betalatBelopp.subtraktion(pris.multiplikation(antalet).addition(moms.multiplikation(antalet)));
        Kvantitet faktisktResultat = betalning.getVaxel();
        assertEquals(presumeratReslutat, faktisktResultat, "Växelbeloppet stämmer ej");
    }
}
package modell;

import verktyg.Kvantitet;

/**
 * Klassen motsvarar en betalning under ett köp
 */
public class Betalning {
    private Kvantitet betalatBelopp;
    private Resultat resultat;

    /**
     * Konstruerar kundens betalade belopp samt summan av priser och momsen på varorna som köps.
     * @param betalatBelopp Det belopp som kunden ger till kassören i kassan, som {@link Kvantitet}
     * @param resultat Det sammanlagda beloppet som kunden ska betala.
     */
    public Betalning(Kvantitet betalatBelopp, Resultat resultat){
        this.betalatBelopp = betalatBelopp;
        this.resultat = resultat;
    }

    /**
     * Skaffar fram det sammanlagda beloppet som kunden ska betala.
     * @return summan av priser och momsen på varorna som köps.
     */
    public Resultat getSumma(){
        return resultat;
    }

    /**
     * Skaffar fram växelbeloppet som {@link Kvantitet}
     * @return värdet av det betalade beloppet subtraherat med det sammanlagda beloppet av priser.
     * och momsen på varorna som {@link Kvantitet}
     */
    public Kvantitet getVaxel(){
        return betalatBelopp.subtraktion(resultat.getSummaInklMoms());
    }
}

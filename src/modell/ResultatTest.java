package modell;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import integrering.Vara;
import integrering.DTO;
import verktyg.Kvantitet;

class ResultatTest {
    private Resultat resultatNoll;

    @BeforeEach
    void setUp() throws Exception {
        resultatNoll = new Resultat();
    }

    @AfterEach
    void tearDown() throws Exception {
        resultatNoll = null;
    }

    @Test
    void getSummaInklMoms() {
        Kvantitet pris = new Kvantitet(100);
        Kvantitet moms = new Kvantitet(25);
        Kvantitet antal = new Kvantitet(3);
        String varaNamnID = "Yxa";
        DTO attributVara = new DTO(pris, varaNamnID, moms);
        Vara vara = new Vara(attributVara, varaNamnID, antal);
        Kvantitet presumeradSummaInklMoms = pris.addition(moms).multiplikation(antal);
        resultatNoll.uppdateraSumma(vara);
        Kvantitet faktiskSummaInklMoms = resultatNoll.getSummaInklMoms();
        assertEquals(presumeradSummaInklMoms, faktiskSummaInklMoms,
                "Den totala summan inlusive moms stämmer ej");
    }

    @Test
    void uppdateraSumma() {
        Kvantitet pris = new Kvantitet(100);
        Kvantitet moms = new Kvantitet(25);
        Kvantitet antal = new Kvantitet(3);
        String varaNamnID = "Yxa";
        DTO attributVara = new DTO(pris, varaNamnID, moms);
        Vara vara = new Vara(attributVara, varaNamnID, antal);
        Kvantitet presumeradSumma = pris.multiplikation(antal);
        Kvantitet presumeradMoms = moms.multiplikation(antal);
        resultatNoll.uppdateraSumma(vara);
        Kvantitet faktiskSumma = resultatNoll.getSumma();
        Kvantitet faktiskMoms = resultatNoll.getSummaMoms();
        assertEquals(presumeradSumma, faktiskSumma,
                "Den totala summan stämmer ej");
        assertEquals(presumeradMoms, faktiskMoms, "Den totala momsen stämmer ej");
    }

    @Test
    void uppdateraSummanAvIntigVara() {
        Kvantitet pris = new Kvantitet(100);
        Kvantitet moms = new Kvantitet(25);
        Kvantitet antal = new Kvantitet(3);
        String varaNamnID = "Yxa";
        DTO attributVara = new DTO(pris, varaNamnID, moms);
        Vara vara = null;
        Kvantitet presumeradSumma = new Kvantitet(0);
        Kvantitet presumeradMoms = new Kvantitet(0);
        resultatNoll.uppdateraSumma(vara);
        Kvantitet faktiskSumma = resultatNoll.getSumma();
        Kvantitet faktiskMoms = resultatNoll.getSummaMoms();
        assertEquals(presumeradSumma, faktiskSumma,
                "Den totala summan stämmer ej");
        assertEquals(presumeradMoms, faktiskMoms, "Den totala momsen stämmer ej");
    }
}

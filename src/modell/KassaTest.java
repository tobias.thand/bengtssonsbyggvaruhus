package modell;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import integrering.Vara;
import integrering.DTO;
import verktyg.Kvantitet;

class KassaTest {

    @Test
    void bifogaBetalning() {
        Kassa kassa = new Kassa();
        Kvantitet pris = new Kvantitet(100);
        Kvantitet moms = new Kvantitet(25);
        String varansNamnID = "Yxa";
        Kvantitet antalet = new Kvantitet(3);
        DTO attributVara = new DTO(pris, varansNamnID, moms);
        Vara vara = new Vara(attributVara, varansNamnID, antalet);
        Resultat resultat = new Resultat();
        resultat.uppdateraSumma(vara);
        Kvantitet betalatBelopp = new Kvantitet(100);
        Betalning betalning = new Betalning(betalatBelopp, resultat);
        kassa.bifogaBetalning(betalning);
        Kvantitet presumeratResultat = new Kvantitet(0).addition(betalning.getSumma().getSummaInklMoms());
        Kvantitet faktisktResultat = kassa.getSaldo();
        assertEquals(presumeratResultat, faktisktResultat,
                "Saldot stämmer ej");
    }
}
package modell;
import integrering.Vara;
import verktyg.Kvantitet;

/**
 * Klassen motsvarar summan av priser och momsen.
 */
public class Resultat {
    private Kvantitet summa;
    private Kvantitet summaMoms;

    /**
     * Konstruerar summa och summan av momsen. Summan börjar på noll i båda fallen.
     */
    public Resultat(){
        this.summa = new Kvantitet(0);
        this.summaMoms = new Kvantitet(0);
    }

    /**
     * Skaffar fram summan av priser som {@link Kvantitet}
     * @return summan som {@link Kvantitet}
     */
    public Kvantitet getSumma(){
        return summa;
    }

    /**
     * Skaffar fram summan av momsen som {@link Kvantitet}
     * @return summan av momsen som {@link Kvantitet}
     */
    public Kvantitet getSummaMoms(){
        return summaMoms;
    }

    /**
     * Skaffar fram summan av priser och momsen som {@link Kvantitet}
     * @return summan av priser och momsen som {@link Kvantitet}
     */
    public Kvantitet getSummaInklMoms(){
        return summa.addition(summaMoms);
    }

    /**
     * Adderar en varas pris och moms, multiplicerat med antalet, till den tidigare summan.
     * @param vara Varans pris och moms, multiplicerat med antalet, läggs till köpets resultat.
     */
    public void uppdateraSumma(Vara vara) {
        if (vara == null) {
            return;
        }

        Kvantitet antalVaror = vara.getAntal();
        Kvantitet varansPris = vara.getVaransAttribut().getPris();
        Kvantitet varansMoms = vara.getVaransAttribut().getVaransMoms();

        this.summaMoms = this.summaMoms.addition(antalVaror.multiplikation(varansMoms));
        this.summa = this.summa.addition(antalVaror.multiplikation(varansPris));
    }
}

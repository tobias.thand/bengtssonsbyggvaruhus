package modell;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import java.time.LocalDateTime;
import integrering.Vara;
import integrering.DTO;
import verktyg.Kvantitet;

class KvittoTest {

    @Test
    void testToString() {
        Kvantitet pris = new Kvantitet(100);
        Kvantitet moms = new Kvantitet(25);
        String varansNamnID = "Yxa";
        Kvantitet antalet = new Kvantitet(3);
        DTO attributVara = new DTO(pris, varansNamnID, moms);
        Vara vara = new Vara(attributVara, varansNamnID, antalet);
        Kop kop = new Kop();
        kop.uppdateraKop(vara);
        Kvitto kvitto = new Kvitto(kop);
        LocalDateTime tidKop = LocalDateTime.now();
        String presumeratResultat = "\n         Bengtssons Byggvaruhus       " +
                "\n*************** Kvitto ***************" +
                "\nTid vid köp: " + tidKop.toLocalDate().toString() +
                "\nVaror: " +
                "\nvarans namn: " + varansNamnID + "\t" +
                "varans pris: " + pris + "\n" +
                "varans moms: " + moms + "\t" +
                " antal: " + antalet + "\nSumma: " + pris.multiplikation(antalet)
                + "\nMoms: " + moms.multiplikation(antalet) + "\n" +
                "\n*********** Välkommen åter! ***********\n";
        String faktisktResultat = kvitto.toString();
        assertEquals(presumeratResultat, faktisktResultat,
                "Kvittots text stämmer ej");
    }
}
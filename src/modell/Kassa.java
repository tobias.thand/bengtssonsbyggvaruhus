package modell;

import verktyg.Kvantitet;

/**
 * Klassen motsvarar ett kassa-system.
 */
public class Kassa {
    private Kvantitet saldo;

    /**
     * Konstruerar ett tillstånd då kassans saldo är noll.
     */
    public Kassa(){
        this.saldo = new Kvantitet(0);
    }

    /**
     * Skaffar fram saldot som {@link Kvantitet}
     * @return Saldot som {@link Kvantitet}
     */
    public Kvantitet getSaldo(){
        return saldo;
    }

    /**
     * När kunden gör en betalning uppdateras kassans saldo.
     * @param betalning Det belopp som adderas till saldot.
     */
    public void bifogaBetalning (Betalning betalning){
        saldo = saldo.addition(betalning.getSumma().getSummaInklMoms());
    }
}

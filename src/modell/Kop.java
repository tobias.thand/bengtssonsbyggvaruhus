package modell;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.HashMap;
import integrering.Vara;

/**
 * Motsvarar utförandet av ett köp i butiken.
 */
public class Kop {
    private Resultat resultat;
    private HashMap<String, Vara> varor = new HashMap<>();

    /**
     * Konstruerar ett köp.
     */
    public Kop(){
        this.resultat = new Resultat();
    }

    /**
     * Skaffar fram summan av priser på varor.
     * @return summan av priser på varor.
     */
    public Resultat getSumma() {
        return resultat;
    }

    /**
     * Skaffar fram varor från en HashMap.
     * @return varor från en HashMap.
     */
    public HashMap<String, Vara> getVaror(){
        return varor;
    }

    /**
     * Uppdaterar antalet varor och summan av priser samt moms.
     * @param vara Varan som ska läggas till köpet.
     * @return Varans egenskaper i form av text (String).
     */
    public String uppdateraKop(Vara vara){
        if(inventarieInnehaller(vara)){
            uppdateraAntalVarorOchSumma(vara);
        } else {
            fogaVaraAntalOchSumma(vara);
        }
        return vara.getVaransAttribut().toString();
    }

    private boolean inventarieInnehaller(Vara vara) {
        return varor.containsKey(vara.getVaransID());
    }

    private void uppdateraAntalVarorOchSumma(Vara vara){
        Vara tidigareVara = varor.get(vara.getVaransID());
        tidigareVara.utvidgaAntal(vara.getAntal());
        varor.put(tidigareVara.getVaransID(), tidigareVara);
        resultat.uppdateraSumma(vara);
    }

    private void fogaVaraAntalOchSumma(Vara vara){
        varor.put(vara.getVaransID(), vara);
        resultat.uppdateraSumma(vara);
    }

    /**
     * Gör text av ett givet köps uppgifter: <code>String</code>
     * @return Det specifika köpet som <code>String</code>
     */
    @Override
    public String toString(){
        StringBuilder builder = new StringBuilder();
        Iterator iterationPoster = getPoster();

        while(iterationPoster.hasNext()){
            Vara vara = getAktuellVara(iterationPoster);
            builder.append(vara.getVaransAttribut().toString());
            fogaNyLinje(builder, " antal: " + vara.getAntal().toString());
        }

        fogaNyLinje(builder, "Summa: " + resultat.getSumma().toString());
        fogaNyLinje(builder, "Moms: " + resultat.getSummaMoms());
        return builder.toString();
    }

    private Iterator getPoster(){
        Set poster = varor.entrySet();
        return poster.iterator();
    }

    private Vara getAktuellVara(Iterator iterationPoster){
        Map.Entry inventering = (Map.Entry) iterationPoster.next();
        return (Vara) inventering.getValue();
    }

    private void fogaNyLinje(StringBuilder builder, String linje){
        builder.append(linje);
        builder.append("\n");
    }
}

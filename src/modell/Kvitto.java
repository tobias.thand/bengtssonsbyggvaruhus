package modell;

import java.time.LocalDateTime;

/**
 * Klassen motsvarar ett kvitto.
 */
public class Kvitto {
    private Kop kop;

    /**
     * Konstruerar ett kvitto
     * @param kop
     */
    public Kvitto(Kop kop){
        this.kop = kop;
    }

    /**
     * Gör text av ett givet köps uppgifter.
     * @return Ett kvitto som <code>String</code>
     */
    @Override
    public String toString(){
        StringBuilder builder = new StringBuilder();
        nyLinje(builder);
        bifogaLinje(builder, "         Bengtssons Byggvaruhus       ");
        bifogaLinje(builder, "*************** Kvitto ***************");
        fogaLokalTid(builder);
        bifogaLinje(builder, "Varor: ");
        bifogaLinje(builder, kop.toString());
        bifogaLinje(builder, "*********** Välkommen åter! ***********");
        return builder.toString();
    }

    private void nyLinje (StringBuilder builder) {
        builder.append("\n");
    }

    private void bifogaLinje (StringBuilder builder, String linje){
        builder.append(linje);
        nyLinje(builder);
    }

    private void fogaLokalTid (StringBuilder builder){
        LocalDateTime tidKop = LocalDateTime.now();
        bifogaLinje(builder, "Tid vid köp: " + tidKop.toLocalDate().toString());
    }
}


package integrering;

import java.util.Objects;
import verktyg.Kvantitet;

/**
 * Representation av varans namn/ID och pris-uppgifter.
 */
public class DTO {
    private final String varansNamn;
    private final Kvantitet varansMoms;
    private final Kvantitet pris;

    /**
     * Konstruerar en varas DTO.
     * @param varansNamn Varas namn.
     * @param pris Varans pris som {@link Kvantitet}
     * @param varansMoms som {@link Kvantitet}
     */
    public DTO(Kvantitet pris, String varansNamn, Kvantitet varansMoms){
        this.pris = pris;
        this.varansNamn = varansNamn;
        this.varansMoms = varansMoms;
    }

    /**
     * Skaffar fram varans namn.
     * @return varanas namn som text (String).
     */
    public String getVaransNamn() { return varansNamn; }

    /**
     * Skaffar fram varans pris som {@link Kvantitet}
     * @return priset som {@link Kvantitet}
     */
    public Kvantitet getPris() { return pris; }

    /**
     * Skaffar fram varans moms som {@link Kvantitet}
     * @return momsen som {@link Kvantitet}
     */
    public Kvantitet getVaransMoms() { return varansMoms; }

    /**
     * Skapar text av den specifika varans uppgifter: <code>String</code>
     * @return En given instans som: <code>String</code>
     */
    @Override
    public String toString(){
        String builder = "varans namn: " + varansNamn + "\t" +
                "varans pris: " + pris + "\n" +
                "varans moms: " + varansMoms + "\t";
        return builder;
    }

    /**
     * Jämför två instanser av klassen DTO som <code>String</code> och returnerar ett booleskt svar.
     * @param objekt Den övriga instansen.
     *       @return <code>false</code> om objektet är intigt,
     *                   om de två intanserna är identiska reurneras <code>true</code>
     *                   annars returneras <code>false</code>
     */
    @Override
    public boolean equals(Object objekt){
        if(objekt == null){
            return false;
        }
        if(getClass() != objekt.getClass()){
            return false;
        }
        final DTO ovrig = (DTO) objekt;
        if(!Objects.equals(this.pris, ovrig.pris)){
            return false;
        }
        if(!this.varansNamn.equals(ovrig.varansNamn)){
            return false;
        }
        if(!Objects.equals(this.varansMoms, ovrig.varansMoms)){
            return false;
        }
        return true;
    }
}

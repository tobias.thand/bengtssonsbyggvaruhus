package integrering;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import verktyg.Kvantitet;

import static org.junit.jupiter.api.Assertions.*;

class DTOTest {
    private DTO testVara;

    @BeforeEach
    void setUp() {
        this.testVara = new DTO( new Kvantitet(100), "Yxa", new Kvantitet(25));

    }

    @AfterEach
    void tearDown() {
        this.testVara = null;
    }

    @Test
    void testToString() {
        String varansNamn = "Yxa";
        Kvantitet pris = new Kvantitet(100);
        Kvantitet moms = new Kvantitet(25);
        DTO DTO = new DTO(pris, varansNamn, moms);
        String presumeradText = "varans namn: " + varansNamn + "\t" +
                "varans pris: " + pris + "\n" + "varans moms: " + moms + "\t";
        String faktiskText = DTO.toString();
        assertEquals(presumeradText, faktiskText,
                "De båda texterna är ej identiska.");
    }

    @Test
    void testToStringIntigMaxMin() {
        String varansNamn = "";
        Kvantitet pris = new Kvantitet(Integer.MAX_VALUE);
        Kvantitet moms = new Kvantitet(Integer.MIN_VALUE);
        DTO DTO = new DTO(pris, varansNamn, moms);
        String presumeradText = "varans namn: " + varansNamn + "\t" +
                "varans pris: " + pris + "\n" + "varans moms: " + moms + "\t";
        String faktiskText = DTO.toString();
        assertEquals(presumeradText, faktiskText,
                "De båda texterna är ej identiska.");
    }

    @Test
    void testEquals() {
        String varansNamn = "Yxa";
        Kvantitet pris = new Kvantitet(100);
        Kvantitet moms = new Kvantitet(25);
        DTO ovrigDTO = new DTO(pris, varansNamn, moms);
        boolean presumeratResultat = true;
        boolean faktisktResultat = testVara.equals(ovrigDTO);
        assertEquals(presumeratResultat, faktisktResultat,
                "De båda ger lika utfall");
    }

    @Test
    void testEqualsEjLika() {
        String varansNamn = "Motorsåg";
        Kvantitet pris = new Kvantitet(100);
        Kvantitet moms = new Kvantitet(25);
        DTO ovrigDTO = new DTO(pris, varansNamn, moms);
        boolean presumeratResultat = false;
        boolean faktisktResultat = testVara.equals(ovrigDTO);
        assertEquals(presumeratResultat, faktisktResultat,
                "De båda ger olika utfall");
    }


    @Test
    void testEqualsObjekt() {
        Object ovrigtObjekt = new Object();
        boolean presumeratResultat = false;
        boolean faktisktResultat = testVara.equals(ovrigtObjekt);
        assertEquals(presumeratResultat, faktisktResultat,
                "Objektet är lika med java.lang.Object.");
    }

    @Test
    void testEqualsIntig() {
        Object ovrigtObjekt = null;
        boolean presumeratResultat = false;
        boolean faktisktResultat = testVara.equals(ovrigtObjekt);
        assertEquals(presumeratResultat, faktisktResultat,
                "Objektet är intigt");
    }
}
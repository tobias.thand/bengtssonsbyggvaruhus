package integrering;

import verktyg.Kvantitet;

/**
 * Representation av en vara
 */
public class Vara {
    private String varansID;
    private DTO varansAttribut;
    private Kvantitet antal;

    /**
     * Varans egenskaper, Namn/ID och antal skapas.
     * @param varansAttribut Varans egenskaper.
     * @param varansID Varans Namn/ID.
     * @param antal Varans antal som {@link Kvantitet}
     */
    public Vara(DTO varansAttribut, String varansID, Kvantitet antal){
        this.varansAttribut = varansAttribut;
        this.varansID = varansID;
        this.antal = antal;
    }

    /**
     * Adderar det tidigare antalet med det nya.
     * @param nyKvantitet Den {@link Kvantitet} som ska adderas.
     */
    public void utvidgaAntal(Kvantitet nyKvantitet){
        this.antal = this.antal.addition(nyKvantitet);
    }

    /**
     * Subtraherar det tidigare antalet med det nya.
     * @param nyKvantitet Den {@link Kvantitet} som ska subtraheras
     */
    public void reduceraAntal(Kvantitet nyKvantitet){
        this.antal = this.antal.subtraktion(nyKvantitet);
    }

    /**
     * Skaffar fram varans antal som {@link Kvantitet}
     * @return Varans antal som {@link Kvantitet}
     */
    public Kvantitet getAntal(){
        return antal;
    }

    /**
     * Skaffar fram varans egenskaper.
     * @return Varans egenskaper
     */
    public DTO getVaransAttribut(){
        return varansAttribut;
    }

    /**
     * Skaffar fram varans namn/ID.
     * @return Varans namn/ID
     */
    public String getVaransID(){
        return varansID;
    }

    /**
     * Skapar text av den specifika varans uppgifter: <code>String</code>
     * @return En given instans som: <code>String</code>
     */
    @Override
    public String toString(){
        String builder = ("varan är: " + varansID) + "antalet: " + antal +
                "specifikation vara: " + varansAttribut.toString();
        return builder;
    }

    /**
     * Jämför två instanser som <code>String</code> och returnerar ett booleskt svar.
     * @param objekt Den övriga instansen.
     *      @return <code>false</code> om objektet är intigt,
     *                 om de två intanserna är identiska reurneras <code>true</code>
     *                 annars returneras <code>false</code>
     */
    @Override
    public boolean equals (Object objekt){
        if (objekt == null){
            return false;
        }
        if (getClass() != objekt.getClass()){
            return false;
        }
        final Vara ovrig = (Vara) objekt;
        if(!this.varansAttribut.equals(ovrig.varansAttribut)){
            return false;
        }
        if(!this.varansID.equals(ovrig.varansAttribut)){
            return false;
        }
        return true;
    }
}

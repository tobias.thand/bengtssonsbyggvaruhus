package integrering;

import java.util.HashMap;
import verktyg.KundID;
import verktyg.Rabatt;

/**
 * Eftersom att det inte finns någon extern databas utgör denna klass ett substitut för en sådan
 */
public class RabattRegister {
    private HashMap<KundID, Rabatt> rabatter = new HashMap<>();
    private final double UTAN_RABATT = 1.0;

    /**
     * Utgör ett substitut för en åtgärd i den externa rabatt-register-databasen
     */
    public RabattRegister(){
        fogaKunderOchDerasRabatt();
    }

    /**
     * Kundens nummer och rabatt identifieras
     * @param kundnummer Identifikation av kundens kundnummer
     * @return I det fall då kundens nummer kan kopplas till en rabatt returneras denna,
     * och det fall då kunden ej har någon rabatt returneras <code>UTAN_RABATT</code>
     */
    public Rabatt kundensRabatt(KundID kundnummer){
        if (rabatter.containsKey(kundnummer)){
            return rabatter.get(kundnummer);
        }
        return new Rabatt(UTAN_RABATT);
    }

    /**
     * Eftersom att det inte finns någon extern databas lagras ett antal exempel på kundnummer,
     * och respektive rabatt här
     */
    public void fogaKunderOchDerasRabatt(){
        rabatter.put(new KundID("10001"), new Rabatt(0.25));
        rabatter.put(new KundID("10002"), new Rabatt(0.4));
        rabatter.put(new KundID("10003"), new Rabatt(0.1));
        rabatter.put(new KundID("10004"), new Rabatt(0.3));
        rabatter.put(new KundID("10005"), new Rabatt(0.5));
    }
}

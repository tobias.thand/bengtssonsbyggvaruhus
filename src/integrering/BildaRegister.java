package integrering;

/**
 * Utgör ett substitut för ett verkligt register/databas.
 */
public class BildaRegister {

    /**
     * Initiation av varu-register - och rabatt-register-system.
     */
    private VaruRegister varuRegister;
    private RabattRegister rabattRegister;

    /**
     * Klassen konstruerar varu-register och rabatt-register.
     */
    public BildaRegister(){
        rabattRegister = new RabattRegister();
        varuRegister = new VaruRegister();
    }

    /**
     * Skaffar fram varu-registrets uppgifter.
     * @return varu-registrets uppgifter.
     */
    public VaruRegister getVaruRegister(){
        return varuRegister;
    }

    /**
     * Skaffar fram rabatt-registrets uppgifter.
     * @return rabatt-registrets uppgifter.
     */
    public RabattRegister getRabattRegister() {
        return rabattRegister;
    }
}

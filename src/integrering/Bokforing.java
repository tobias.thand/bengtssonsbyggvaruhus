package integrering;

import java.time.LocalDateTime;
import java.util.HashMap;
import modell.Kop;

/**
 * Utgör ett substitut för ett externt bokföringssytem.
 */
public class Bokforing {
    private HashMap<LocalDateTime, Kop> bokforing = new HashMap();

    /**
     * Utgör ett substitut för en åtgärd i det externt bokföringssytemet.
     */
    public Bokforing(){}

    /**
     * Köpet bokas i bokföringsystemet.
     * Bokningen av köpet kopplas till datum och klockslag.
     * @param kop Köpet bokas.
     */
    public void bokning (Kop kop){
        LocalDateTime tidForKop = LocalDateTime.now();
        bokforing.put(tidForKop, kop);
    }
}


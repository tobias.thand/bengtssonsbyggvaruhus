package integrering;

import java.util.HashMap;
import verktyg.Kvantitet;

/**
 * Eftersom att det inte finns någon extern databas för varu-register utgör denna klass ett substitut för en sådan.
 */
public class VaruRegister {
    private HashMap<String, DTO> varuRegister = new HashMap<>();

    /**
     * Utgör ett substitut för en åtgärd i den externa varu-register-databasen.
     */
    public VaruRegister(){ fogaVaror(); }

    /**
     * Tar reda på ifall en vara i - den fiktiva - databasen är disponibel.
     * @param urskiljVara Identifikation av vara
     * @return Om varan är disponibel <code>true</code> annars <code>false</code>
     */
    public boolean disponibelVara (String urskiljVara) {
        return varuRegister.containsKey(urskiljVara);
    }

    /**
     * Skaffar fram varans uppgifter och antal.
     * @param urskiljVara Identifikation av vara.
     * @param antal Varans antal.
     * @return Om varan ej är disponibel returneras null, annars returneras uppgifter och antal.
     */
    public Vara getVara (String urskiljVara, Kvantitet antal){
        if(disponibelVara(urskiljVara)){
            return new Vara(varuRegister.get(urskiljVara), urskiljVara, antal);
        }
        return null;
    }

    /**
     * Eftersom att det inte finns någon extern databas lagras ett antal exempel på varor här.
     */
    private void fogaVaror(){
        varuRegister.put("Yxa", new DTO(new Kvantitet(100), "Yxa",  new Kvantitet(25)));
        varuRegister.put("Morakniv", new DTO(new Kvantitet(50), "Morakniv",  new Kvantitet(12)));
        varuRegister.put("Såg", new DTO(new Kvantitet(300), "Såg",  new Kvantitet(75)));
        varuRegister.put("Spade", new DTO(new Kvantitet(150), "Spade",  new Kvantitet(37)));
        varuRegister.put("Skruvmejsel", new DTO(new Kvantitet(80), "Skruvmejsel",  new Kvantitet(20)));
        varuRegister.put("Hammare", new DTO(new Kvantitet(125), "Hammare",  new Kvantitet(57)));
    }
}

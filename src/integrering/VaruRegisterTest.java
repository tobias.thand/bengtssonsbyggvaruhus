package integrering;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class VaruRegisterTest {


    @Test
    void disponibelVara() {
        String yxa = "Yxa";
        VaruRegister varuRegister = new VaruRegister();
        boolean presumeratResultat = true;
        boolean faktisktResultat = varuRegister.disponibelVara(yxa);
        assertEquals(presumeratResultat, faktisktResultat,
                "Varan har ej funnits");
    }

    @Test
    void ejDisponibelVara() {
        String yxa = "Motorsåg";
        VaruRegister varuRegister = new VaruRegister();
        boolean presumeratResultat = false;
        boolean faktisktResultat = varuRegister.disponibelVara(yxa);
        assertEquals(presumeratResultat, faktisktResultat,
                "Varan har ej funnits");
    }

    @Test
    void intigDisponibelVara() {
        String yxa = null;
        VaruRegister varuRegister = new VaruRegister();
        boolean presumeratResultat = false;
        boolean faktisktResultat = varuRegister.disponibelVara(yxa);
        assertEquals(presumeratResultat, faktisktResultat,
                "Varan har ej funnits");
    }
}
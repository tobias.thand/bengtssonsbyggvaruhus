package integrering;
import modell.Kvitto;
import modell.Kop;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

import verktyg.Kvantitet;

public class UtskriftTest {
    private PrintStream systemUt;
    private ByteArrayOutputStream stoffUt;

    @BeforeEach
    void setUp() {
        systemUt = System.out;
        stoffUt = new ByteArrayOutputStream();
        System.setOut(new PrintStream(stoffUt));
    }

    @AfterEach
    void tearDown() {
        stoffUt = null;
        System.setOut(systemUt);
    }

    @Test
    void skrivUtKvitto() {
        Kvantitet pris = new Kvantitet(100);
        Kvantitet moms = new Kvantitet(25);
        String varansNamnOchID = "Yxa";
        DTO varansAttribut = new DTO(pris, varansNamnOchID, moms);
        Kvantitet antalet = new Kvantitet(3);
        Vara vara = new Vara(varansAttribut, varansNamnOchID, antalet);
        Kop kop = new Kop();
        kop.uppdateraKop(vara);
        Kvitto kvitto = new Kvitto(kop);
        LocalDateTime tidKop = LocalDateTime.now();
        String presumeratResultat = "\n         Bengtssons Byggvaruhus       " +
                "\n*************** Kvitto ***************" +
                "\nTid vid köp: " + tidKop.toLocalDate().toString() +
                "\nVaror: " +
                "\nvarans namn: " + varansNamnOchID + "\t" +
                "varans pris: " + pris + "\n" +
                "varans moms: " + moms + "\t" +
                " antal: " + antalet + "\nSumma: " + pris.multiplikation(antalet)
                + "\nMoms: " + moms.multiplikation(antalet) + "\n" +
                "\n*********** Välkommen åter! ***********\n";
        String faktisktResultat = kvitto.toString();
        assertEquals(presumeratResultat, faktisktResultat, "De två fallen är ej lika");
    }
}
package integrering;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import verktyg.Kvantitet;

class VaraTest {
    private Vara treYxor;

    @BeforeEach
    void setUp() {
        treYxor = new Vara(new DTO(new Kvantitet(100), "Yxa", new Kvantitet(25)),
                "Yxa", new Kvantitet(3));
    }

    @AfterEach
    void tearDown() {
        treYxor = null;
    }

    @Test
    void utvidgaAntal() {
        Kvantitet summaA = new Kvantitet(3);
        Kvantitet summaB = new Kvantitet(2);
        Kvantitet presumeradSumma = summaA.addition(summaB);
        treYxor.utvidgaAntal(summaB);
        Kvantitet resultatSumma = treYxor.getAntal();
        assertEquals(presumeradSumma, resultatSumma,
                "Det fall då antalat utvidgas stämer ej " +
                        "överens med uträkningen i java-klassen Kvantitet");
    }

    @Test
    void utvidgaAntalMedNegativtTal() {
        Kvantitet summaA = new Kvantitet(3);
        Kvantitet summaB = new Kvantitet(-7);
        Kvantitet presumeradSumma = summaA.addition(summaB);
        treYxor.utvidgaAntal(summaB);
        Kvantitet resultatSumma = treYxor.getAntal();
        assertEquals(presumeradSumma, resultatSumma,
                "Det fall då antalat utvidgas stämer ej " +
                        "överens med uträkningen i java-klassen Kvantitet");
    }

    @Test
    void utvidgaAntalMedMAXVALUE() {
        Kvantitet summaA = new Kvantitet(3);
        Kvantitet summaB = new Kvantitet(Integer.MAX_VALUE);
        Kvantitet presumeradSumma = summaA.addition(summaB);
        treYxor.utvidgaAntal(summaB);
        Kvantitet resultatSumma = treYxor.getAntal();
        assertEquals(presumeradSumma, resultatSumma,
                "Det fall då antalat utvidgas stämer ej " +
                        "överens med uträkningen i java-klassen Kvantitet");
    }

    @Test
    void utvidgaAntalMedMINVALUE() {
        Kvantitet summaA = new Kvantitet(3);
        Kvantitet summaB = new Kvantitet(Integer.MIN_VALUE);
        Kvantitet presumeradSumma = summaA.addition(summaB);
        treYxor.utvidgaAntal(summaB);
        Kvantitet resultatSumma = treYxor.getAntal();
        assertEquals(presumeradSumma, resultatSumma,
                "Det fall då antalat utvidgas stämer ej " +
                        "överens med uträkningen i java-klassen Kvantitet");
    }

    @Test
    void reduceraAntal() {
        Kvantitet reduceras = new Kvantitet(3);
        Kvantitet reducerasMed = new Kvantitet(1);
        Kvantitet presumeradSumma = reduceras.subtraktion(reducerasMed);
        treYxor.reduceraAntal(reducerasMed);
        Kvantitet resultatSumma = treYxor.getAntal();
        assertEquals(presumeradSumma, resultatSumma,
                "Det fall då antalat reduceras stämer ej " +
                        "med uträkningen i java-klassen Kvantitet");
    }

    @Test
    void reduceraAntalMedNegativtTal() {
        Kvantitet reduceras = new Kvantitet(3);
        Kvantitet reducerasMed = new Kvantitet(-7);
        Kvantitet presumeradSumma = reduceras.subtraktion(reducerasMed);
        treYxor.reduceraAntal(reducerasMed);
        Kvantitet resultatSumma = treYxor.getAntal();
        assertEquals(presumeradSumma, resultatSumma,
                "Det fall då antalat reduceras stämer ej " +
                        "med uträkningen i java-klassen Kvantitet");
    }

    @Test
    void reduceraAntalMedMAXVALUE() {
        Kvantitet reduceras = new Kvantitet(3);
        Kvantitet reducerasMed = new Kvantitet(Integer.MAX_VALUE);
        Kvantitet presumeradSumma = reduceras.subtraktion(reducerasMed);
        treYxor.reduceraAntal(reducerasMed);
        Kvantitet resultatSumma = treYxor.getAntal();
        assertEquals(presumeradSumma, resultatSumma,
                "Det fall då antalat reduceras stämer ej " +
                        "med uträkningen i java-klassen Kvantitet");
    }

    @Test
    void reduceraAntalMedMINVALUE() {
        Kvantitet reduceras = new Kvantitet(3);
        Kvantitet reducerasMed = new Kvantitet(Integer.MIN_VALUE);
        Kvantitet presumeradSumma = reduceras.subtraktion(reducerasMed);
        treYxor.reduceraAntal(reducerasMed);
        Kvantitet resultatSumma = treYxor.getAntal();
        assertEquals(presumeradSumma, resultatSumma,
                "Det fall då antalat reduceras stämer ej " +
                        "med uträkningen i java-klassen Kvantitet");
    }

    @Test
    void testToString() {
        Kvantitet pris = new Kvantitet(100);
        Kvantitet moms = new Kvantitet(25);
        String varansNamn = "Yxa";
        DTO DTO = new DTO(pris, varansNamn, moms);
        Kvantitet antal = new Kvantitet(3);
        Vara varaSomSkaTestas = new Vara(DTO, varansNamn, antal);
        String presumeradText = "varan är: " + varansNamn + "antalet: " + antal +
                "specifikation vara: " + DTO.toString();
        String resultatText = varaSomSkaTestas.toString();
        assertEquals(presumeradText, resultatText,"Textarna stämmer överens");
    }

    @Test
    void testToStringEjSamma() {
        Kvantitet pris = new Kvantitet(95);
        Kvantitet moms = new Kvantitet(20);
        String varansNamn = "Motorsåg";
        DTO DTO = new DTO(pris, varansNamn, moms);
        Kvantitet antal = new Kvantitet(3);
        Vara varaSomSkaTestas = new Vara(DTO, varansNamn, antal);
        String presumeradText = "varan e: " + varansNamn + "så många: " + antal +
                "förklaring: " + DTO.toString();
        String resultatText = varaSomSkaTestas.toString();
        assertNotEquals(presumeradText, resultatText,"Textarna stämmer överens");
    }

    @Test
    void testToStringIntig() {
        Kvantitet pris = new Kvantitet(0);
        Kvantitet moms = new Kvantitet(0);
        String varansNamn = "";
        DTO DTO = new DTO(pris, varansNamn, moms);
        Kvantitet antal = new Kvantitet(0);
        Vara varaSomSkaTestas = new Vara(DTO, varansNamn, antal);
        String presumeradText = "";
        String resultatText = varaSomSkaTestas.toString();
        assertNotEquals(presumeradText, resultatText,"Textarna stämmer överens");
    }

    @Test
    void testEqualsEjLika() {
        Kvantitet pris = new Kvantitet(95);
        Kvantitet moms = new Kvantitet(15);
        String varansNamn = "Motorsåg";
        DTO DTO = new DTO(pris, varansNamn, moms);
        Kvantitet antal = new Kvantitet(1);
        Vara varaSomSkaTestas = new Vara(DTO, varansNamn, antal);
        boolean presumeratResultat = false;
        boolean faktisktResultat = treYxor.equals(varaSomSkaTestas);
        assertEquals(presumeratResultat, faktisktResultat,
                "Det ena fallet är identiskt med det andra");
    }

    @Test
    void testEqualsIntigt() {
        Vara varaSomSkaTestas = null;
        boolean presumeratResultat = false;
        boolean faktisktResultat = treYxor.equals(varaSomSkaTestas);
        assertEquals(presumeratResultat, faktisktResultat,
                "Fallet är intigt");
    }

    @Test
    void testEqualsObjekt() {
        Object objekt = new Object();
        boolean presumeratResultat = false;
        boolean faktisktResultat = treYxor.equals(objekt);
        assertEquals(presumeratResultat, faktisktResultat,
                "Fallet är identiskt med java-objektet");
    }

    @Test
    void testEqualsEjSammaID() {
        Kvantitet pris = new Kvantitet(100);
        Kvantitet moms = new Kvantitet(25);
        String varansNamn = "Yxa";
        DTO DTO = new DTO(pris, varansNamn, moms);
        Kvantitet antal = new Kvantitet(3);
        Vara varaSomSkaTestas = new Vara(DTO, varansNamn, antal);
        boolean presumeratResultat = false;
        boolean faktisktResultat = treYxor.equals(varaSomSkaTestas);
        assertEquals(presumeratResultat, faktisktResultat,
                "Trots olika varu-ID är båda fallen identiska");
    }

    @Test
    void testEquals() {
        Kvantitet pris = new Kvantitet(100);
        Kvantitet moms = new Kvantitet(25);
        Kvantitet antal = new Kvantitet(3);
        DTO nyDTO = new DTO(pris, "Yxa", moms);
        Vara testVara = new Vara(nyDTO, "Yxa", antal);
        boolean presumeratResultat = false;
        boolean faktisktResultat = treYxor.equals(testVara);
        assertEquals(presumeratResultat, faktisktResultat,
                "Det ena fallet är ej identiskt med det andra");
    }
}
package integrering;

import modell.Kvitto;

/**
 * Klassen motsvarar en skrivare.
 */
public class Utskrift {
    private Utskrift utskrift;

    /**
     * Utgör ett substitut för en utskrift av en skrivare.
     */
    public Utskrift(){
    }

    /**
     * Motsvarar utskrift av kvitto från en extern skrivare.
     * Utskriften sker till <code>System.out.println</code> eftersom att extern skrivare saknas
     * @param kvitto En instans av Kvitto skrivs ut.
     */
    public void skrivUtKvitto(Kvitto kvitto){
        System.out.println(kvitto.toString());
    }
}

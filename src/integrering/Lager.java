package integrering;

import java.util.HashMap;
import java.util.Set;
import java.util.Map;

import modell.Kop;
import verktyg.Kvantitet;

/**
 * Eftersom att lagrets databas ej existerar utgör denna klass ett substitut för en sådan.
 */
public class Lager {
    private HashMap<String, Vara> lager = new HashMap();

    /**
     * Utgör ett substitut för en åtgärd i den externa lager-databasen.
     */
    public Lager(){
        fogaVaror();
    }

    /**
     * Uppdaterar det kvarvarande antalet av olika varor i lagret då ett köp genomförs.
     * @param kop Köpets uppgifter innehåller varornas uppgifter.
     */
    public void uppdateraLager(Kop kop){
        HashMap<String, Vara> varor = kop.getVaror();
        Set poster = varor.entrySet();
        for(Object entry : poster){
            Vara vara = getAktuellVara(entry);

            if(varaPaLager(vara)){
                reduceraVaransAntal(vara);
            }
        }
    }

    private boolean varaPaLager(Vara vara) {

        return lager.containsKey(vara.getVaransID());
    }

    private void reduceraVaransAntal(Vara vara){
        Vara tidigareVara = lager.get(vara.getVaransID());
        tidigareVara.reduceraAntal(vara.getAntal());
        lager.put(tidigareVara.getVaransID(), tidigareVara);
    }

    private Vara getAktuellVara(Object post){
        Map.Entry inventering = (Map.Entry) post;
        return (Vara) inventering.getValue();
    }

    private void fogaVaror(){
        lager.put("Yxa", new Vara(new DTO( new Kvantitet(100),"Yxa",  new Kvantitet(25)), "Yxa",
                new Kvantitet(2147483647)));
        lager.put("Morakniv", new Vara(new DTO( new Kvantitet(50),"Morakniv", new Kvantitet(12)),
                "Morakniv", new Kvantitet(2147483647)));
        lager.put("Såg", new Vara(new DTO( new Kvantitet(300),"Såg", new Kvantitet(75)), "Såg",
                new Kvantitet(2147483647)));
        lager.put("Spade", new Vara(new DTO(new Kvantitet(150), "Spade", new Kvantitet(37)),
                "Spade", new Kvantitet(2147483647)));
        lager.put("Skruvmejlsel", new Vara(new DTO(new Kvantitet(80), "Skruvmejsel", new Kvantitet(20)),
                "Skruvmejsel", new Kvantitet(2147483647)));
        lager.put("Hammare", new Vara(new DTO(new Kvantitet(125), "Hammare",  new Kvantitet(57)),
                "Hammare", new Kvantitet(2147483647)));
    }
}

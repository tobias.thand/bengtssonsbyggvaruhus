package integrering;

/**
 * Initiation av bokföring- och lager-system.
 * Utgör ett substitut för ett verkligt system/databas.
 */
public class BildaSystem {
    private Bokforing bokforing;
    private Lager lager;

    /**
     * Konstruerar bokforing- och lager-sysem.
     */
    public BildaSystem(){
        bokforing = new Bokforing();
        lager = new Lager();
    }

    /**
     * Skaffar fram bokföringens uppgifter.
     * @return bokföringens uppgifter.
     */
    public Bokforing getBokforing() {
        return bokforing;
    }

    /**
     * Skaffar fram lagrets uppgifter.
     * @return lagrets uppgifter.
     */
    public Lager getLager() {
        return lager;
    }
}

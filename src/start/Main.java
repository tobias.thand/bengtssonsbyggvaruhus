package start;

import kontroll.Kontroll;
import integrering.BildaRegister;
import integrering.Utskrift;
import integrering.BildaSystem;
import vy.Vy;

/**
 * Klass för Main-metoden: <code>main</code>
 * Startar programmet.
 */
public class Main {
    /**
     * Startar BildaSystem, Utskrift, BildaRegister, Kontroll och Vy.
     * @param args Tar ej emot parametrar.
     */
    public static void main(String[] args){
        BildaSystem bildaSystem = new BildaSystem();
        Utskrift utskrift = new Utskrift();
        BildaRegister bildaRegister = new BildaRegister();
        Kontroll kontroll = new Kontroll(bildaSystem, utskrift, bildaRegister);
        Vy vy = new Vy(kontroll);
        vy.exempel();
    }
}
